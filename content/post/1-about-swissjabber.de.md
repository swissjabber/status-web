---
title: "Über Swissjabber"
date: 2013-02-03T00:00:00+02:00
draft: false
---

Ein Auszug aus der [WaybackMachine](https://web.archive.org/web/20130203040040/http://www.swissjabber.ch/index.php/About_us):

Swissjabber ist heute ein populärer, mittelgrosser Jabber-Server, welcher primär von
Benutzern aus Deutschland und der Schweiz genutzt wird.

Die Geschichte von Swissjabber begann im August 2002, als Marco Balmer (alias Micressor)
die Technologie Jabber für sich entdeckte. Am 25.11.2002 war es soweit, alle
Initialarbeiten waren getan und der Server ging auf einem eingemieteten Server in Basel
ans Netz.

Bis heute hat das Projekt einiges an technischen Änderungen durchgemacht. Nicht weniger
als 3 mal wurde Swissjabber auf andere Hardware umgezogen, 4x wurde die Website komplett
umgebaut bis die für uns optimale Variante gefunden war.

Als Server-Software leistete uns [jabberd14](https://web.archive.org/web/20130203040040/http://jabberd.org/) (dies war die erste Open Source Jabber-Server-Software überhaupt) 6
Jahre lang sehr gute Dienste. Im April 2009 migrierten wir den Server auf
[ejabberd](https://web.archive.org/web/20130208143313/http://www.ejabberd.im:80/)
(-startschuss für das Projekt ejabberd war interessanterweise am 16.09.2002).

Seit Februar 2008 sind wir zurück in die Schweiz [gezogen](https://web.archive.org/web/20130203040040/http://www.swissjabber.ch/index.php/20071231_Swissjabber_move_out_to_Switzerland). Etwas über drei Jahre wurde der Server in Deutschland betrieben. Das Alter dieses
Servers, wie auch der Vorratsdatenspeicherungs-Wahn der EU, haben uns zum Rückzug in die
sicherere Schweiz gezwungen.

Seit dem Umzug hat [Nine](https://www.nine.ch/) als neuer Connectivity-Partner mit dem
qualifizierten Support und der Freude über das technische Handwerk Marco Balmer überzeugt.

Aus diesem Grund war ein weiterer Schritt naheliegend. Nine hat deshalb den Swissjabber
Dienst ab Februar 2011 übernommen und ist ab sofort der neue Ansprechpartner für
sämtliche Belangen.
