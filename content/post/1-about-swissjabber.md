---
title: "About Swissjabber"
date: 2013-02-03T00:00:00+02:00
draft: false
---

Today Swissjabber is a popular, medium sized Jabber server, which is primarily operated by
users from Germany and Switzerland.

The story of Swissjabber began in August 2002, when Marco Balmer (aka Micressor)
discovered Jabber's technology. On 25.11.2002 it was so far, all
Initial work was done and the server was running on a rented server in Basel.
to the grid.

To date, the project has undergone a number of technical changes. No less
Swissjabber was moved to different hardware more than 3 times, 4x the website was completed
until the optimal variant for us was found.

As server software we used [jabberd14](https://web.archive.org/web/20130203040040/http://jabberd.org/) (this was the first open source jabber server software ever) 6
very good service for many years. In April 2009 we migrated the server to
[ejabberd](https://web.archive.org/web/20130208143313/http://www.ejabberd.im:80/)
(-starting signal for the project ejabberd was interestingly on 16.09.2002).

Since February 2008 we  [moved](https://web.archive.org/web/20130203040040/http://www.swissjabber.ch/index.php/20071231_Swissjabber_move_out_to_Switzerland) back to Switzerland.
The server was operated in Germany for just over three years. The age of this
servers, as well as the EU's data retention mania, have led us to withdraw to the
safer Switzerland.

Since the move,[Nine](https://www.nine.ch/), as a new connectivity partner with the
Marco Balmer is convinced by the qualified support and the joy of the technical trade.

For this reason, a further step was obvious. Nine therefore has the Swissjabber
service from February 2011 and is now the new contact for
all issues.

