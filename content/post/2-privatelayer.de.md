---
title: "PRIVATE LAYER übernimmt Hosting für Swissjabber"
date: 2017-04-01T00:00:00+02:00
author: Nine / Private Layer
draft: false
---

**Wer ist PRIVATE LAYER?**

Private Layer (AS51852 / privatelayer.com), mit Rechenzentren und Hosting-Betrieben in
Schweiz, bietet eine breite Palette von Offshore-Privatsphäre-Hosting-Produkten, die
entwickelt wurden den technischen und budgetären Anforderungen internationaler Kunden
gerecht zu werden, die Wert auf Privatsphäre und Stabilität legt.

**Warum Nine Swissjabber an PRIVATE LAYER übergeben hat**

nine.ch, als führender Anbieter von Managed Linux Servern in der Schweiz, befindet sich
im Prozess, um unser aktuelles Produktportfolio neu zu definieren, um den
zukünftigen Kunden- und Marktanforderungen gerecht zu werden. Während dieses Prozesses
haben wir erkannt, dass die Swissjabber-Lösung nicht mehr im Einklang mit unserem
zukünftigen Geschäftsstrategie steht und deshalb haben wir uns entschlossen, eine neue
Heimat für den Swissjabber zu suchen. Mit PRIVATE LAYER haben wir eine neue Oase für die
Swissjabber Plattform gefunden.

**Willkommen bei PRIVATE LAYER**

PRIVATE LAYER begrüsst die Swissjabber-Community und wird die ursprüngliche Idee von
Swissjabber, eine kostenlose und offene Messaging-Plattform zur Verfügung zu stellen,
die den Chat mit mehreren und sogar mit Chat-Diensten anderer Anbieter wie Google Talk,
GMX, etc funktioniert. Die offizieller Ansprechpartner für alle Fragen und
Support-Anfragen ist ab sofort PRIVATE LAYER Inc. für weitere Details auf privatelayer.com.
