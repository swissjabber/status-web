---
title: "Swissjabber stays offline"
date: 2018-11-22T18:44:06+01:00
---

All former Swissjabber users have surely found a new Jabber home by now.

The Jabber server was unavailable since December 2017. Therefore, I do not think it makes sense anymore to resume server operation.

The story can be found [here](/2018/07/swissjabber-founder-takes-over-domains-and-database/).

Swissjabber stays offline - Thank you for your loyalty!

Tell us your thoughts on Twitter  under hashtag
[#swissjabber](https://twitter.com/hashtag/swissjabber?f=tweets&vertical=default) or via
[E-Mail](mailto:swissjabber AT secure DOT mailbox DOT org).

Greetings,\
Marco
