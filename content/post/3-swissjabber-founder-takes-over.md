---
title: "Swissjabber founder takes over domains and database"
date: 2018-07-09T11:30:00+02:00
draft: false
---

At the end of 2017 I got emails from contacts of my time with
Swissjabber. *What happened to Swissjabber?* and the hint,
that the service has been completely unavailable for some time.

With the help of [Nine](https://www.nine.ch) (thank you!!!!) it was possible to
get in touch with the current operator **PRIVATE LAYER**.

Here are the facts:

* The server was shut down sometime in early 2018.
* I am back in possession of most Swissjabber domains.
* I have received a copy of the Prosody based Swissjabber database.
* PRIVATE LAYER will no longer host the service.
* There is currently no server or hoster for the service.

As the founder of Swissjabber, it has always been important to me to be transparent.
Unfortunately, this has not been achieved since the takeover by PRIVATE LAYER.

The goal of [this website](https://status.swissjabber.ch):

* Inform existing and former Swissjabber users.
* So that Swissjabber can continue to exist if necessary.


Tell us your thoughts on Twitter
[#swissjabber](https://twitter.com/hashtag/swissjabber?f=tweets&vertical=default).
