---
title: "Swissjabber bleibt offline"
date: 2018-11-22T18:44:06+01:00
---

Alle ehemaligen Swissjabber-Nutzende haben inzwischen sicher eine neue Jabber-Heimat gefunden.

Der Jabber-Server war seit Dezember 2017 nicht mehr erreichbar. Daher halte ich es inzwischen
nicht mehr für sinnvoll, den Serverbetrieb wieder aufzunehmen.

Die Geschichte dazu findet Ihr [hier](/de/2018/07/swissjabber-gründer-übernimmt-domains-und-datenbank/).

Swissjabber bleibt offline - Danke für Deine Treue!

Sag uns Deine Meinung auf Twitter unter dem Hashtag [#swissjabber](https://twitter.com/hashtag/swissjabber?f=tweets&vertical=default) oder via
[E-Mail](mailto:swissjabber AT secure DOT mailbox DOT org).

Gruss,\
Marco
