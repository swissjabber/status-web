---
title: "PRIVATE LAYER takes over hosting for Swissjabber"
date: 2017-04-01T00:00:00+02:00
author: Nine / Private Layer
draft: false
---

**Who is PRIVATE LAYER**

Private Layer (AS51852 / privatelayer.com), with data centers and hosting operations in
Switzerland, offers a wide range of offshore privacy hosting products that are designed
to meet the technical and budgetary needs of international customers that value privacy
and stability.

**Why nine handovers Swissjabber to PRIVATE LAYER**

nine.ch, as the leading provider of managed Linux servers in Switzerland, is in the
process of redefining their efforts and strengths across our current product portfolio,
in order to meet the future customer and market demands. During this process, we’ve
recognized that the Swissjabber solution is no longer in-line with our future core
business strategy and therefore we’ve decided to look for a new home of the Swissjabber
community. With PRIVATE LAYER we’ve found a new haven for the Swissjabber chatting
platform.

**Welcome to PRIVATE LAYER**

PRIVATE LAYER welcomes the Swissjabber community and will continue the original idea of
Swissjabber, to provide a free open messaging platform that allows chat with multiple
users and even with chat-services from other providers like Google Talk, GMX, etc. The
official contact for any questions and support needs is as of now PRIVATE LAYER Inc., see
for further details on privatelayer.com.
