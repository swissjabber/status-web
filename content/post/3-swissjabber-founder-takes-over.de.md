---
title: "Swissjabber Gründer übernimmt Domains und Datenbank"
date: 2018-07-09T11:30:00+02:00
draft: false
---

Ende 2017 erhielt ich von einigen Kontakten aus meinen Zeiten mit
Swissjabber eine Email. *Was ist mit Swissjabber passiert?* und dem Hinweis,
dass der Service seit einiger Zeit überhaupt nicht mehr erreichbar ist.

Mit der Hilfe von [Nine](https://www.nine.ch) (vielen Dank!!!) ist es mir
inzwischen gelungen, mit dem derzeitgen Betreiber **PRIVATE LAYER** Kontakt
aufzunehmen.

Hier die Fakten:

* Der Server wurde irgendwann zu Beginn des 2018 ausgeschaltet.
* Ich bin wieder im Besitz der meisten Swissjabber Domains.
* Ich habe eine Kopie der Prosody basierten Swissjabber Datenbank erhalten.
* PRIVATE LAYER wird den Service nicht länger hosten.
* Es gibt derzeit keinen Server- oder Hoster für den Service.

Als Gründer von Swissjabber war es mir immer wichtig transparent zu
kommunizieren. Leider ist dies seit der Übernahme durch PRIVATE LAYER
nicht mehr gelungen.

Das Ziel [dieser Website](https://status.swissjabber.ch):

* Bestehende und ehemalige Swissjabber-Nutzende zu informieren.
* Damit Swissjabber bei Bedarf weiter bestehen kann.

Sag uns Deine Meinung auf Twitter unter dem Hashtag
[#swissjabber](https://twitter.com/hashtag/swissjabber?f=tweets&vertical=default).
